SHELL = /bin/sh
export ROOTS_VERSION=master

export PROJECT_PATH=$(shell pwd)
export AWS_SHARED_CREDENTIALS_FILE=$(PROJECT_PATH)/aws/credentials
export AWS_CONFIG_FILE=$(PROJECT_PATH)/aws/config

AWS_OUTPUT=table

-include $(shell [ ! -d .roots ] && git clone --branch $(ROOTS_VERSION) https://gitlab.com/skalesys/roots.git .roots; echo .roots/Makefile)

.PHONY: install
install:
	@$(SELF) terraform/install

.PHONY: aws/describe/vpcs
aws/describe/vpcs:
	@( \
		source $(ROOTS_VENV_PATH)/bin/activate && \
		aws ec2 describe-vpcs --output $(AWS_OUTPUT) \
	)

.PHONY: aws/describe/subnets
aws/describe/subnets:
	@( \
		source $(ROOTS_VENV_PATH)/bin/activate && \
		aws ec2 describe-subnets --output $(AWS_OUTPUT) \
	)

.PHONY: aws/describe/instances
aws/describe/instances:
	@( \
		source $(ROOTS_VENV_PATH)/bin/activate && \
		aws ec2 describe-instances --output $(AWS_OUTPUT) \
	)
