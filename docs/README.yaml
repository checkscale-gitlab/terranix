---

name: terranix

logo: docs/logo.jpeg

pipeline_status: "false"

commercial_support: "false"

repo: skalesys/terranix

description: |-
  Automate your AWS environment with Terraform and LinuxAcademy.

screenshots:
  - name: "demo"
    description: "How to initialize project using [roots][roots]"
    url: "docs/screenshots/demo.gif"

requirements:
  - name: "Ubuntu"
    url: "ubuntu"
    description: "Ubuntu is a complete Linux operating system, freely available with both community and professional support."

  - name: "Ansible"
    url: "ansible"
    description: "Ansible is the simplest way to automate apps and IT infrastructure."

  - name: "Terraform"
    url: "terraform"
    description: "Write, Plan, and Create Infrastructure as Code"

  - name: "Packer"
    url: "packer"
    description: "Build Automated Machine Images"

  - name: "Docker"
    url: "docker"
    description: "Securely build, share and run any application, anywhere"

usage: |-
  1) Fork this project
  2) Start an AWS Sandbox via Linux Academy
  3) Copy and paste the newly AWS Access Key ID and Secret Key into the directory `aws/credentials`

  ```
  # To initialize the project
  make init

  # Possible values for environment can be: development, testing, staging, production
  make terraform/install terraform/plan ENVIRONMENT=development

  ```

quickstarts:
  - command: "make init"
    description: "to initialize the [roots][root]"

examples:
  - name: "terraform-null-label"
    url: "https://github.com/cloudposse/terraform-null-label/"
    description: "A terraform module that leverages `terraform/%` targets"

related_projects:
  - name: "Packages"
    url: "https://github.com/cloudposse/packages"
    description: "Cloud Posse installer and distribution of native apps"

  - name: "Dev Harness"
    url: "https://github.com/cloudposse/dev"
    description: "Cloud Posse Local Development Harness"

references:
  - name: "Wikipedia - Test Harness"
    description: 'The `build-harness` is similar in concept to a "Test Harness"'
    url: "https://en.wikipedia.org/wiki/Test_harness"

resources:
  - name: "Photo"
    url: "https://unsplash.com/photos/OHOU-5UVIYQ"
    description: "Photo by SpaceX on Unsplash"

  - name: "Gitignore.io"
    url: "https://gitignore.io"
    description: "Defining the `.gitignore`"

  - name: "LunaPic"
    url: "https://www341.lunapic.com/editor/"
    description: "Image editor (used to create the avatar)"

links:
  - name: "aws"
    url: "https://aws.amazon.com/"

contributors:
  - name: "Valter Silva"
    username: "valter-silva"

# Other files to include in this README from the project folder
include:
  - "docs/targets.md"